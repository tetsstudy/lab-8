#include "file_reader.h"
#include "constants.h"

#include <fstream>
#include <cstring>




void read(const char* file_name, RoseWind* array[], int& size)
{
	std::ifstream file(file_name);
	if (file.is_open())
	{
		size = 0;
		char tmp_buffer[MAX_STRING_SIZE];
		while (!file.eof())
		{
			RoseWind* item = new RoseWind;
			file >> item->day;
			file >> item->month;
			file >> item->direction;
			//file.read(tmp_buffer, 1); // ������ ������� ������� �������	
			file >> item->speed;
					
			array[size++] = item;
		}
		file.close();
	}
	else
	{
		throw "������ �������� �����";
	}
}