#pragma once

#ifndef WIND_ROSE_H
#define WIND_ROSE_H


#include "constants.h"

struct RoseWind {
	int day;
	int month;
	char direction[MAX_STRING_SIZE];
	double speed;
};
#endif