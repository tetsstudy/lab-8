#include <iostream>
#include <iomanip>

using namespace std;

#include "wind_rose.h"
#include "file_reader.h"
#include "constants.h"

RoseWind** Specail_Direction (RoseWind* array[], int size, 
	bool(*check)(RoseWind* element),
	int& result_size
	);

/*
�������� ������� <function_name>:
������� ���������� ������ � ��������� ������� � ��� ��������� �� ��������,
��� ������� ������� ������ ���������� �������� true, ���������� � �����
������, ��������� �� ������� ������������ ��������

���������:
array       - ������ � ��������� �������
size        - ������ ������� � ��������� �������
check       - ��������� �� ������� ������.
� �������� �������� ����� ��������� ����� �������� ���
������� ������, �������� ������� ������� ����
result_data - ��������, ������������ �� ������ - ����������, � �������
������� ������� ������ ��������������� �������

������������ ��������
��������� �� ������ �� ���������� �� ��������, ��������������� �������
������ (��� ������� ������� ������ ���������� true)
*/

bool check_deriction(RoseWind * element);
bool check_speed (RoseWind* element	);

/*
�������� ������� <check_function_name>:
������� ������ - ���������, ������������� �� ���� ������� ������� ������

���������:
element - ��������� �� �������, ������� ����� ���������

������������ ��������
true, ���� ������� ������������� ������� ������, � false � ���� ������
*/

int main()
{

	setlocale(LC_ALL, "Russian");
	cout << "������������ ������ �8. GIT\n";
	cout << "������� �6. ���� ������\n";
	cout << "�����: ����� ������\n\n";
	RoseWind* subscriptions[MAX_FILE_ROWS_COUNT];
	int size;
	try
	{
		read("data.txt", subscriptions, size);
		cout << "***** ���� ������ *****\n\n";
		
		for (int i = 0; i < size; i++)
		{
			/********** ����� �������� **********/
			cout << "���� ";
			// ����� ���
			cout << subscriptions[i]->day << ".";
			// ����� ������
			cout << subscriptions[i]->month << "\n";
			
			/********** ����� ����������� � �������� **********/
			cout << "����������� � ��������...........: ";
			// ����� �����������
			cout << subscriptions[i]->direction << " ";
			// ����� ��������
			cout << subscriptions[i]->speed;
			
			cout << '\n';
			cout << '\n';
		}

		int choose = 0;
		cin >> choose;
		int result_size;
		RoseWind** subscriptions2;
		if (choose == 1)
			subscriptions2 = Specail_Direction(subscriptions, size, check_deriction, result_size);
		else
			subscriptions2 = Specail_Direction(subscriptions, size, check_speed, result_size);


		for (int i = 0; i < result_size; i++)
		{
			/********** ����� �������� **********/
			cout << "���� ";
			
			// ����� ���
			cout << subscriptions2[i]->day << ".";
			// ����� ������
			cout << subscriptions2[i]->month << "\n";

			/********** ����� ����������� � �������� **********/
			cout << "����������� � ��������...........: ";
			// ����� �����������
			cout << subscriptions2[i]->direction << " ";
			// ����� ��������
			cout << subscriptions2[i]->speed;

			cout << '\n';
			cout << '\n';
		}

		/*for (int i = 0; i < result_size; i++)
			delete[] subscriptions2[i];*/
		delete[] subscriptions2;

		for (int i = 0; i < size; i++)
		{
			delete subscriptions[i];
		}
	}
	catch (const char* error)
	{
		cout << error << '\n';
	}
	system("pause");
	return 0;
}

RoseWind ** Specail_Direction(RoseWind * array[], int size, bool(*check)(RoseWind *element), int & result_size)
{
	RoseWind **result = new RoseWind*[size];
	result_size = 0;

	for (int i = 0; i < size; ++i)
		if (check(array[i]))
		{
			result[i] = new RoseWind;
			result[result_size++] = array[i];
		}

	return result;
}

bool check_speed(RoseWind * element)
{
	if (element->speed > 5)
		return true;
	return false;
}

bool check_deriction(RoseWind * element)
{
	// West, NorthWest ��� North.
	if (!strcmp(element->direction, "West") || !strcmp(element->direction, "NorthWest") || !strcmp(element->direction, "North"))
		return true;
	return false;
}
